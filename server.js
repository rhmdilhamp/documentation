var express = require('express');
var app = express();
var port = process.env.PORT || 3000;
var morgan = require('morgan');
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var router = express.Router();
var routerAccess = express.Router();
var passport = require('passport');
var social = require('./app/passport/passport')(app, passport);
var fs = require('fs');
var jwt = require('jsonwebtoken');
var { secret } = require('./app/config/index');
var path = require('path');


// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: true }));
var userRouter = require('./app/routes/user');
var userAccessRouter = require('./app/routes/userAccess');
var providerRouter = require('./app/routes/provider');
var attributeRouter = require('./app/routes/get');
var paymentRouter = require('./app/routes/payment');
var discussionRouter = require('./app/routes/discussion');

    //view engine
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'ejs');

    app.use(function (req, res, next) {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, DELETE, POST');
        res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, content-type, Accept, Authorization');
        res.setHeader('Access-Control-Allow-Credentials', true);
        // bypass option method
        if('OPTIONS'==req.method) {
            res.send(200);
        }else{
            next();
        }
    });

  app.use(morgan('dev'));
  app.use(bodyParser.json({limit: '10mb'}));
  app.use(bodyParser.urlencoded({limit: '10mb',extended: true}));
  //app.use(express.multipart());

  app.use('/v1/user', userRouter);
  app.use('/get', attributeRouter);
  app.use('/v1/user', paymentRouter);


//Route verifiy Token
// app.use(function(req, res, next) {

// var token = req.body.token || req.body.query || req.headers['x-access-token'];

// if (token) {
//     //verify token
//     jwt.verify(token, secret, function(err, decoded){
//         if (err) {
//             res.json({ success: false, message: 'Token Invalid' });
//         } else {
//             req.decoded = decoded.id;
//             next();
//         }
//     });
// } else {
//     res.json({ success: false, message: 'No token provided' });
// }   

// });

// --- JWT Validaltion ---
app.use(function(req, res, next){
	if(req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer'){
		var token = req.headers.authorization.split(' ')[1];
		jwt.verify(token, secret, function(err, decoded){
	    if (err){
  			return res.json({ success: false, message: 'Failed to authenticate token.' });
  		}
  		else{
                req.user_id=decoded.id;
                req.id_user=decoded.id_user;
                //req.role=decoded.role;
	  			req.token=jwt.sign({
                        id:decoded.id,
                        id_user:decoded.id_user,
                        //role:decoded.role,
                        name:decoded.name,
                        email:decoded.email
                        } ,secret, { expiresIn : '24h' });
	  	next();
  		}
		})
	}
	else{
  	return res.status(400).json({ status:400, message: 'Please send token' });
  }
});


// app.post('/auth', function(req, res){
//     res.send(req.user_id);
// });

app.use('/v1/user', userAccessRouter);
app.use('/v1/provider', providerRouter);
app.use('/v1/discussion', discussionRouter);

mongoose.connect('mongodb://localhost:27017/backendtravinesia', function(err){
    if (err){
        console.log('Not connected to the database: ' + err);
    } else {
        console.log('Successfully connected to MongoDB');
    }
});


app.listen(port, function(){
    console.log('Running the server on port ' + port);
});